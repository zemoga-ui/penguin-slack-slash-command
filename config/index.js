module.exports = Object.assign({
  DATE_FORMAT: 'YYYYMMDD',
  PENGUIN_REPORT_API: 'https://uitraining.zemoga.com/penguin-report/api',
  PENGUIN_REPORT_URL: 'https://uitraining.zemoga.com/penguin-report',
  SLACK_USERS_URL: 'https://slack.com/api/users.list',
  SLACK_TOKEN: '',
  DEPARTMENTS: {
    'G8D0PGE8G': 'APPDEV',
    'C03PD1ZS0': 'UI',
    'G3ZVCAM33': 'PMO',
    'C96R28AP5': 'QA',
    'G0EQHS64V': 'CREATIVE'
  },
  FINAL_REPORT: false,
}, process.env);
