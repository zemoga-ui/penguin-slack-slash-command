const earlyFridays = require('./constants/earlyFridays');
const getLastBusinessDay = require('./utils/getLastBusinessDay');
const { isEarlyFriday: checkEarlyFriday } = require('./utils/earlyFriday');
const CONFIG = require('../config');

async function dateInfo() {
  const responseObj = {
    statusCode: 200
  };

  const lastBusinessDay = await getLastBusinessDay();

  const isEarlyFriday = checkEarlyFriday(lastBusinessDay);

  responseObj.body = JSON.stringify({
    lastBusinessDay: lastBusinessDay.format('DD/MM/YYYY'),
    earlyFridays,
    isEarlyFriday,
  });

  return responseObj;
};

module.exports = dateInfo;
