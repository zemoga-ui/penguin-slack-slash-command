/**
 * Events Entrypoint
 * Handles and redirects triggers when actions happen in Slack
 *
 * NOTE: Currently, conversational intents are parsed manually.
 * Eventually we would like to add support for Lex or other
 * conversational APIs
 */
const get = require('lodash.get');
const CONFIG = require('../config');
const respondMessage = require('./message');
const { parseBody } = require('./utils/slack');
const basecampToken = require('./basecampToken');
const dateInfo = require('./dateInfo');

module.exports = {
    handler: function(evt, ctx, callback) {
      const responseObj = {
        statusCode: 200
      };
      const slackBody = parseBody(evt.body);
      const text = get(slackBody, 'text');
      const askingToday = text && text.indexOf('today') >= 0;

      console.log('text ', text, ' asking Today', askingToday);

      if (askingToday) {
        const todayResponse = {};
        todayResponse['response_type'] = 'in_channel';
        todayResponse.text = `Asking for today's report`;

        responseObj.body = JSON.stringify(todayResponse);
      }

      console.log(responseObj);

      callback(null, responseObj);

      const channelId = get(slackBody, 'channel_id');

      return respondMessage(evt.body, {
        channelId,
        askingToday
      });
    },
    scheduled: function(evt, ctx, callback) {
      const responseObj = {
        statusCode: 200
      };
      const departmentsChannels = Object.keys(CONFIG.DEPARTMENTS);

      callback(null, responseObj);

      return Promise.all(departmentsChannels.map(channelId => {
        return respondMessage(evt.body, {
          channelId,
          scheduled: true
        });
      }));
    },
    lastCheck: function(evt, ctx, callback) {
      const responseObj = {
        statusCode: 200
      };
      const departmentsChannels = Object.keys(CONFIG.DEPARTMENTS);

      callback(null, responseObj);

      return Promise.all(departmentsChannels.map(channelId => {
        return respondMessage(evt.body, {
          channelId,
          scheduled: true,
          lastCheck: true
        });
      }));
    },
    basecampToken,
    dateInfo,
};
