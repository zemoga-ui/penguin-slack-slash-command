const moment = require('moment-timezone');
const business = require('moment-business');
const fetch = require('node-fetch');
const get = require('lodash.get');
const { getHolidays, isHoliday } = require('./utils/holidays');
const { getUsersIds, parseBody } = require('./utils/slack');
const MEMES = require('./constants/memes');
const { isEarlyFriday } = require('./utils/earlyFriday');
const CONFIG = require('../config');

/**
 * Get data from Basecamp and sends the message
 * @param {Object} - Post params
 * @return {Promise} - Fetch data promise
 */
function sendMessage(postData, {
    channelId,
    askingToday,
    scheduled = false,
    lastCheck = false
  }) {
    const slackBody = parseBody(postData);
    const responseUrl = get(slackBody, 'response_url');
    const department = CONFIG.DEPARTMENTS[channelId];
    const today =  moment().tz('US/Central');
    const lastBusinessDay = askingToday ? today.clone() : business.subtractWeekDays(today.clone(), 1);
    let sendLast = false;
    let usersIds = {};
    let scheduledUrl = `https://slack.com/api/chat.postMessage?token=${CONFIG.SLACK_TOKEN}&channel=${channelId}`;

    console.log('id and department', channelId, department);
    console.log('asking for today', askingToday);

    if (!department) {
      return fetch(responseUrl, {
        method: 'POST',
        body: JSON.stringify({
          text: 'This command is only available in specific channels'
        }),
        headers:{
          'Content-Type': 'application/json'
        }
      });
    }

    return getUsersIds()
        .then((users) => {
          usersIds = users;

          return getHolidays();
        })
        .then((holidays) => {

          // Checking if message will be send
          if (scheduled) {
            if (!business.isWeekDay(today) || isHoliday(holidays, today)) {
              return false;
            }
          }

          while (isHoliday(holidays, lastBusinessDay)) {
            if (askingToday) {
              business.addWeekDays(lastBusinessDay, 1);
            } else {
              business.subtractWeekDays(lastBusinessDay, 1);
            }
          }

          const formattedLastBusinessDay = lastBusinessDay.format(CONFIG.DATE_FORMAT);
          const { PENGUIN_REPORT_API: api } = CONFIG;

          console.log('Fetch from Penguin API', `${api}?date=${formattedLastBusinessDay}&department=${department}`);
  
          return fetch(`${api}?date=${formattedLastBusinessDay}&department=${department}`);
        })
        .then((res) => res.json())
        .then(data => {
            let message = '';

            if (!data) {
                message =
                    'Hubo un error al llamar al API de Penguin Report. 😅';
            }

            const early = isEarlyFriday(lastBusinessDay);
            const minimunHours = early ? 6 : 7;

            if (scheduled && !lastCheck) {
              message += '*SCHEDULED MESSAGE* \n\n';
            }

            if (lastCheck) {
              message += CONFIG.FINAL_REPORT ? '*FINAL REPORT* \n\n' : '*LAST REMINDER* \n\n';
            }

            if (early) {
              message += '*EARLY FRIDAY CHECKING 6 HOURS* \n\n';
            }

            const penguined = data
              .filter((peep) => peep.totalHours < minimunHours)
              .map((peep) => {
                const slackUser = `<@${usersIds[get(peep, 'googleProfile.email')]}>`

                return `🐧  *${peep['person-name']} ${slackUser}* (${peep.totalHours} horas)`;
              });

            if (penguined.length) {
                message +=
                    'Las siguientes personas aún no han reportado horas:\n\n' +
                    penguined.join('\n\n') +
                    '\n\n---\n\n' +
                    CONFIG.PENGUIN_REPORT_URL +
                    '?date=' +
                    lastBusinessDay.format('YYYY-MM-DD');
                message = message.replace('Maria Antonia Serna', 'ANTONIA!!!!');
                message = message.replace('Andres Garcia', 'OSCARRR!!!!');
                message = message.replace('Carlos Suarez', 'Quién es Carlos?');

                if (department === 'PMO') {
                  message += '\n\n https://media.giphy.com/media/gyYJYyCPezUWI/giphy.gif';
                }

                sendLast = true;
            } else {
                var currentMeme = 'https://vignette.wikia.nocookie.net/adventuretimewithfinnandjake/images/e/eb/Lol_the_simpsons_party_hard_gif.gif/revision/latest?cb=20131123141855';
                var memesRand = MEMES[department];

                if (memesRand) {
                  let randNum = Math.floor((Math.random() * memesRand.length));
                  currentMeme = memesRand[randNum];
                }

                message =
                    '¡Hemos Cumplido! 🙌 \n\n' +
                    currentMeme;
            }

            return message;
        })
        .then((msg) => {

          const data = {
            "response_type": "in_channel",
            text: msg
          };

          if (responseUrl) {
            return fetch(responseUrl, {
              method: 'POST',
              body: JSON.stringify(data),
              headers:{
                'Content-Type': 'application/json'
              }
            });
          } else {
            console.log('Final msg', msg);

            if (scheduled) {

              // Do not send message if not needed
              if (lastCheck && !sendLast) {
                return;
              }

              scheduledUrl += `&text=${encodeURI(data.text)}&icon_emoji=${encodeURI(':clock10:')}`;

              return fetch(scheduledUrl, {
                method: 'POST',
                headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json; charset=utf-8'
                }
              })
              .then(response => response.json())
              .then(res => console.log(res));
            }

            return false;
          }
        })
        .catch(e => {
            console.log('error', e);
        });
}

module.exports = sendMessage;
