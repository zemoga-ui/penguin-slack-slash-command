const fetch = require('node-fetch');
const get = require('lodash.get');
const CONFIG = require('../../config');

function getUsersIds() {
  return fetch(`${CONFIG.SLACK_USERS_URL}?token=${CONFIG.SLACK_TOKEN}`)
    .then((res) => res.json())
    .then((response) => {
        const members = get(response, 'members');
        const users = {};

        members.forEach((member) => {
          const email = get(member, 'profile.email');

          users[email] = member.id;
        });

        return users;
    })
    .catch((err) => {
        debug.error(err);
        return Promise.resolve([]);
    });
}

module.exports = {
  getUsersIds,
  parseBody(codedString = '') {         
    var pairs = codedString.split('&');
    
    var result = {};
    pairs.forEach(function(pair) {
        pair = pair.split('=');
        result[pair[0]] = decodeURIComponent(pair[1] || '');
    });
    return JSON.parse(JSON.stringify(result));
  }
};
