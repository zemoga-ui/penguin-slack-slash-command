const moment = require('moment-timezone');
const earlyFridays = require('../constants/earlyFridays');

module.exports = {
  isEarlyFriday(date) {
    const dates = earlyFridays.map(h => moment(h));

    return !!dates.find(h => h.isSame(date, 'day'));
  }
};
