const moment = require('moment-timezone');
const CONFIG = require('../../config');
const holidays = require('../constants/holidays');

/**
 * Call the Holidays API to get the years holidays
 * @param   {string|Moment} date - Year as string or Moment Object
 * @return  {Promise} - Fetch Promise
 */
function getHolidays() {
    console.log('Holiday cache found. Not calling the API');

    return Promise.resolve(holidays.map(h => moment(h.date)));
}

module.exports = {
    getHolidays,
    isHoliday(holidays, date) {
        if (!holidays) {
            return false;
        }

        return Boolean(holidays.find(h => h.isSame(date, 'day')));
    },
    nextHolidayFrom(requestDate) {
        return getHolidays(requestDate).then(holidays => {
            let holiday;

            if (holidays) {
                holiday = holidays.find(h =>
                    h.isSameOrAfter(requestDate, 'day')
                );

                console.log(
                    `Next Colombian Holiday:`,
                    holiday.format(CONFIG.DATE_FORMAT)
                );

                return holiday;
            }
        });
    }
};
