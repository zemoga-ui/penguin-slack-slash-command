const moment = require('moment-timezone');
const business = require('moment-business');
const { isHoliday, getHolidays } = require('./holidays');

const getLastBusinessDay = async () => {
  const today =  moment().tz('US/Central');

  const lastBusinessDay = business.subtractWeekDays(today.clone(), 1);
  const holidays = await getHolidays();

  while (isHoliday(holidays, lastBusinessDay)) {
    business.subtractWeekDays(lastBusinessDay, 1);
  }

  return lastBusinessDay;
}

module.exports = getLastBusinessDay;
