module.exports = {
  'UI': [
    "https://media.giphy.com/media/5hvKwbaIHx7kzPjDGf/giphy.gif",
    "https://1.bp.blogspot.com/-cQ0b6am0ckc/WIEbGsVjH2I/AAAAAAAAAMI/8W0wdpRdGNYCOfU2I1TD3NK2mIbqiI3QACLcB/s320/hemoscumplido.jpg",
    "https://media.giphy.com/media/fdG5PIezLlZA9dd50N/giphy.gif",
    "https://media.giphy.com/media/Mo12qktv1OaQ4rQgh8/giphy.gif",
    "https://media.giphy.com/media/ZvwVajTnlO1IBZ8bxl/giphy.gif",
    "https://media.giphy.com/media/Oj7AniCGiqsYzE5ODW/giphy.gif",
    "https://media.giphy.com/media/7JKLVMR4lohVPeuuuP/giphy.gif"
  ],
  'PMO': [
    'https://media.giphy.com/media/6UWZ61yysjnjy/giphy.gif',
    'https://media.giphy.com/media/1vRCeaHbgATwA/giphy.gif',
    'https://media.giphy.com/media/3oEjHOoleGnLc5mIko/giphy.gif',
    'https://media.giphy.com/media/Ou2K91gSK6cTe/giphy.gif'
  ],
  'QA': [
    'https://media.giphy.com/media/pP3f88JZeOPKMzYgsB/giphy.gif',
    'https://i.imgur.com/VQDwzNl.png',
    'https://i.imgur.com/FJ4hC0O.jpg',
    'https://i.imgur.com/Xr8vbVn.jpg',
    'https://i.imgur.com/oYxiti7.jpg',
    'https://media.giphy.com/media/9JkbeJ6An68Got7cSQ/giphy.gif',
    'https://content.screencast.com/users/MauroRodriguez/folders/Jing/media/8f2c15b1-beee-4738-bda9-5778880e40e0/00001195.png',
    'https://i.imgur.com/GHitLXw.jpg'
  ]
};
