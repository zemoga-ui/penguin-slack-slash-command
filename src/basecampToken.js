const fetch = require('node-fetch');

const BASECAMP_TOKEN_URL = 'https://launchpad.37signals.com/authorization/token';

async function basecampToken(evt, ctx, callback) {
  const responseObj = {
    statusCode: 200
  };

  const {
    client_id,
    client_secret,
  } = evt.queryStringParameters;

  const result = await fetch(`${BASECAMP_TOKEN_URL}?${evt.body}&client_id=${client_id}&client_secret=${client_secret}&type=web_server`, {
    method: 'POST',
  }).then(response => response.json());

  result.token_type = 'bearer';

  responseObj.body = JSON.stringify(result);

  return responseObj;
}

module.exports = basecampToken;
