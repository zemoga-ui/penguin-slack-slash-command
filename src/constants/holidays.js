module.exports = [
  { date: '2019-01-01', name: "New Year's Day" },
  { date: '2019-01-07', name: 'Epiphany (observed)' },
  { date: '2019-03-25', name: "St. Joseph's Day" },
  { date: '2019-04-18', name: 'Maundy Thursday' },
  { date: '2019-04-19', name: 'Good Friday' },
  { date: '2019-05-01', name: 'Labour Day' },
  { date: '2019-06-03', name: 'Ascension Day (observed)' },
  { date: '2019-06-24', name: 'Corpus Christi (observed)' },
  { date: '2019-07-01', name: 'Sacred Heart' },
  { date: '2019-07-20', name: 'Declaration of Independence' },
  { date: '2019-08-07', name: 'Battle of Boyacá' },
  { date: '2019-08-19', name: 'Assumption Day' },
  { date: '2019-10-14', name: 'Columbus Day' },
  { date: '2019-11-04', name: 'All Saints Day (observed)' },
  { date: '2019-11-11', name: 'Independence of Cartagena' },
  { date: '2019-12-08', name: 'Immaculate Conception Day' },
  { date: '2019-12-25', name: 'Christmas Day' }
];
